# Installation Instructions: Local Installation with Docker
## Install Docker Dependencies (Docker)

* https://docs.docker.com/install/#desktop
  * NOTE: You will need to create an account (https://hub.docker.com/)

### Change Memory in Prefences Menu to at least 8GB
https://docs.docker.com/docker-for-mac/#preferences-menu

### Test Docker

NOTE: You will not be able to login without an account (https://hub.docker.com/)

```
docker login
docker rm -f rstudio; docker run -d --name rstudio -p 8787:8787 -t cannin/pathway-analysis:0.4
```

Then go to (in your browser's address bar):

127.0.0.1:8787

You should see RStudio and username/password is rstudio/rstudio

## Download Code/Tutorial/Data Files
Download and unzip the zip files in the code and tutorial links below.

### Download (Code)
Download this file:

```
https://bitbucket.org/cbio_mskcc/ludwig_crispr/get/master.zip
```

It will save as cbio_mskcc-ludwig_crispr-xxxxx.zip where xxxxx is some auto-generated suffix. Unzip it.

### Download (Tutorial)
Download this file:

```
https://bitbucket.org/cbio_mskcc/network_analysis_tutorial/get/master.zip
```

It will save as cbio_mskcc-network_analysis_tutorial-xxxxx.zip where xxxxx is some auto-generated suffix. Unzip it.


### Download (Data)
Download folder contents using the "Download All" button in the upper right-hand side: https://drive.google.com/drive/folders/FOLDER_ID
NOTES: 1. FOLDER_ID in the URL is incorrect; the correct link was sent to you in an email, 2. This is about a 1 GB download and may take time.

![download_gdrive](download_gdrive.png)

#### For Advanced Users: Via Command-Line Download Files (Most Users Should IGNORE)
Download using wget
```
wget -nv https://bitbucket.org/cbio_mskcc/ludwig_crispr/get/master.zip -O ~/Downloads/ludwig_crispr_code.zip; unzip ludwig_crispr_code.zip

wget -nv https://bitbucket.org/cbio_mskcc/network_analysis_tutorial/get/master.zip -O ~/Downloads/network_analysis_tutorial.zip; unzip network_analysis_tutorial.zip
```

Alternative download method using gdrive (https://github.com/prasmussen/gdrive)
```
gdrive download --recursive FOLDER_ID --path ludwig_crispr_data
```

## For Advanced Users: Build Docker Container (Most Users Should IGNORE)
```
docker build -t cannin/pathway-analysis:0.4 -f Dockerfile .
```

## Run Docker Container
### Edit Docker Command

In the command below,

1. Change the path "/Users/user/Downloads/" in "/Users/user/Downloads/ludwig_crispr_luna_loos" to where you have downloaded the Google Drive folder location and make sure you have the right name of the "ludwig_crispr_luna_loos" folder
2. Change the path of "/Users/user/Downloads/cbio_mskcc-ludwig_crispr-xxxxx" to the downloaded ludwig_crispr code folder (downloaded from BitBucket) and make sure the folder name is correct
3. Change the path "/Users/user/Downloads/network_analysis_tutorial" to the downloaded network_analysis_tutorial folder (downloaded from BitBucket) and make sure the folder name is correct

```
docker rm -f rstudio; docker run -d --name rstudio -p 8787:8787 \
  -v /Users/user/Downloads/ludwig_crispr_luna_loos:/home/rstudio/crispr_data \
  -v /Users/user/Downloads/cbio_mskcc-ludwig_crispr-xxxxx:/home/rstudio/crispr_code \
  -v /Users/user/Downloads/cbio_mskcc-network_analysis_tutorial-xxxxx:/home/rstudio/network_analysis_tutorial \
  -t cannin/pathway-analysis:0.4
```

### Run Docker Command
Run the above comand with the paths fixed. NOTE: You should see something like the below. As long, as you got the long string of numbers and letters it should have worked.

```
Error: No such container: rstudio
4773096e3a9cb03b2877f10515bef6373d0e320b2edb923f9230d7a6c0649648
```

Then go to (in your browser's address bar):

127.0.0.1:8787

In RStudio, you should have content in each of the 3 folders: crispr_data, crispr_code, and network_analysis_tutorial

### Troubleshooting Notes

* Do not change anything after the colon, then run the command.
* Make sure the command above is run as one line (no line breaks)

## Run Analysis Reports
In the Dockerized RStudio:

1. Click the "crispr_code" folder
2. Click the "ludwig_crispr.Rproj" file
3. Click the "crispr_netboxr.Rmd" file (NOTE: this is as an example to run the NetBoxR analysis. For other analyses click on the corresponding Rmd file).
4. Run the file using the Run All menu in RStudio.

![rstudio_run_menu](rstudio_run_menu.png)

# Installation: Local Installation without Docker

Dockerized installation is **STRONGLY** for first time users.

## Required Packages

* CRAN/diptest
* CRAN/randomForest
* CRAN/readxl
* CRAN/VennDiagram
* CRAN/data.table
* CRAN/plyr
* CRAN/igraph
* CRAN/parcor
* CRAN/RColorBrewer
* CRAN/extremevalues (GitHub: cannin/extremevalues a fork without Tk dependency)
* BioConductor/ComplexHeatmap
* BioConductor/genefilter
* GitHub/cannin/pheatmap
* GitHub/BioPAX/paxtoolsr
* GitHub/cannin/rcausalpath

### Installation of rcellminerDataPackages from Google Drive

Change to each folder and install in order:

1. rcellminer
2. rcellminerData
3. rcellminerUtils
4. ccleData
5. ctrpData

using library(devtools); install(".")

## Installation
This installs packages listed in r-requirements.txt
```
# Set up to use CRAN
setRepositories(ind=1:6)
if(is.null(getOption("repos"))) { options(repos="http://cran.rstudio.com/") }
if(!all(c("devtools", "stringr") %in% rownames(installed.packages()))) { install.packages(c("devtools", "stringr")) }

source("https://gist.githubusercontent.com/cannin/6b8c68e7db19c4902459/raw/installPackages.R")
installPackages("r-requirements.txt")
```
