FROM cannin/rstudio-server:r-3.4.3_rstudio-server-1.1.447

COPY rcellminerDataPackages /rcellminerDataPackages
COPY r-requirements.txt /r-requirements.txt

## Install additional dependencies
RUN export $(cat /etc/environment) \
  && Rscript -e "options(repos = c(CRAN='$MRAN'), download.file.method = 'libcurl'); source('https://gist.githubusercontent.com/cannin/6b8c68e7db19c4902459/raw/installPackages.R'); installPackages(file='/r-requirements.txt', repos='$MRAN')"

RUN echo "crisprDataDir <- '/home/rstudio/crispr_data'\ntutorialDir <- '/home/rstudio/network_analysis_tutorial'" >> /home/rstudio/.Rprofile

EXPOSE 8787

CMD /init
