# Purpose: Extract out drug-target interactions for dgidb in a SIF format 

dat <- read.table("interactions_030818.tsv", sep="\t", header=TRUE, stringsAsFactors = FALSE, comment.char = "", quote = "")

idx <- grepl(".*inhibit.*", dat$interaction_types)

filteredDat <- dat[idx, c("drug_claim_primary_name", "interaction_claim_source", "gene_name")]
filteredDat <- filteredDat[which(filteredDat$gene_name != ""), ]

tmp <- data.frame(PARTICIPANT_A=filteredDat$drug_claim_primary_name, 
                  INTERACTION_TYPE="chemical-affects", 
                  PARTICIPANT_B=filteredDat$gene_name, 
                  INTERACTION_DATA_SOURCE=filteredDat$interaction_claim_source, 
                  stringsAsFactors = FALSE)

write.table(tmp, "dgidbDrugTargets_formatted_030818.txt", col.names = TRUE, row.names = FALSE, quote = FALSE, sep="\t")