---
title: "CRISPR Tables"
output:
  html_notebook: default
---

```{r knitrSetup, include=FALSE}
library(knitr)
opts_chunk$set(out.extra='style="display:block; margin: auto"', fig.align="center", tidy=FALSE)
verbose <- FALSE
```

# Purpose

We analyze the 10 TCGA pathways based on the genes invovled in the pathway, considering 
1) mutations (CCLE data)
2) essentiality (Achilles CRISPR screen)
3) sensitivity to drugs (CTRP drug data)
4) neighboring genes (PathwayCommons)

# Load Libraries

```{r loadLibraries, message=FALSE}
library(readxl)
library(pheatmap)
library(plyr)
library(rcellminer)
library(ctrpData)
```

# Load CRISPR data 

```{r loadDataCRISPR}
# (crisprDataDir  folder path necessary)
# crisprDataDir <- "/Users/user/Dropbox/ludwig_crispr"
source('./R/getCRISPRdata.R')
```

## Extract cell lines of interest

We are interested in the 26 breast cancer cell lines measured in the Achilles CRISPR screen.

```{r extractCcls}
# different parts of the data used in this notebook
dataCRISPRBreast <- dataCRISPR[,which(cclsCRISPR$tissue=="BREAST")]
colnames(dataCRISPRBreast)
```

## Load 10 TCGA pathwayGenes

```{r pathwayGenes}
pathwayGenes <- readRDS("./data/pathwayGenes.rds")
names(pathwayGenes)

rownamesTable <- NULL
for(i in 1:length(pathwayGenes)){
  rownamesTable[i] <- paste(names(pathwayGenes)[i], " (",length(pathwayGenes[[i]]),")",sep="")
}
```

# Generate tables

## Mutation

We compare the TCGA pathways across the cell lines with respect to the number of genes which have a mutation which is known to be actionable (according to OnkoKB.org). 

### Load the CCLE mutation data

```{r cclemutdata}
dataMUT <- read.table(file.path(crisprDataDir, 'cellline_ccle_broad/data_mutations_extended.txt'),header = TRUE,sep = "\t",fill = TRUE,stringsAsFactors = FALSE,quote = "",skip = 1, comment.char = "")
```

### Load actionable variants

```{r actionable}
dataActionable <- read.table('./data/allActionableVariants.txt',header = TRUE,sep = "\t",fill = TRUE,stringsAsFactors = FALSE,quote = "", comment.char = "")
dataAnnotated <- read.table('./data/allAnnotatedVariants.txt',header = TRUE,sep = "\t",fill = TRUE,stringsAsFactors = FALSE,quote = "", comment.char = "")
```

### Fill table

```{r mutation}
# initialization
mutation <- matrix(0,length(pathwayGenes),ncol(dataCRISPRBreast))

rownames(mutation) <- rownamesTable
colnames(mutation) <- colnames(dataCRISPRBreast)

# columns of interest
colMUT = which(colnames(dataMUT)=='Variant_Classification')
colMAFImpact = which(colnames(dataMUT)=='MA.FImpact')
colChange = which(colnames(dataMUT)=='amino_acid_change') 
colCellline = which(colnames(dataMUT) == "Tumor_Sample_Barcode")

# parse cell line names  
dataMUT[,colCellline] <- gsub("_.*","", dataMUT[,colCellline])

for(i in 1:length(pathwayGenes)){
  for(j in 1:ncol(dataCRISPRBreast)){
    for(k in 1:length(pathwayGenes[[i]])){
      
      # filter out missense mutations
      iRows <- which(dataMUT[,1]==pathwayGenes[[i]][k] & dataMUT[,colCellline]==colnames(dataCRISPRBreast)[j])
      if("Missense_Mutation" %in% dataMUT[iRows,colMUT]) {

        rowMissense = which(dataMUT[iRows,colMUT]=="Missense_Mutation")
        missmutations <- dataMUT[iRows[rowMissense],colChange]
        
        for(l in 1:length(missmutations)){
          if(length(!is.na(which(dataActionable$Gene == pathwayGenes[[i]][k] & dataActionable$Alteration==gsub("p.","",missmutations[l]))))>0){
              ind <- which(dataActionable$Alteration==gsub("p.","",missmutations[l]))
              print(paste("Actionable alteration:", dataActionable$Alteration[ind[1]], "in cell line", colnames(dataCRISPRBreast)[j], "and gene", pathwayGenes[[i]][k]))
              mutation[i,j] <- mutation[i,j] + 1
          }else if(length(!is.na(which(dataActionable$Gene == pathwayGenes[[i]][k] & dataActionable$Alteration=="Oncogenic Mutations")))>0){
            ind <- which(dataAnnotated[,4]==pathwayGenes[[i]][k] & paste("p.",dataAnnotated[,5],sep="") == missmutations[l])
            oncogenicity <- dataAnnotated[which(dataAnnotated[,4]==pathwayGenes[[i]][k] & paste("p.",dataAnnotated[,5],sep="") == missmutations[l]),6]
            if(length(oncogenicity)>0){
              for(m in 1:length(oncogenicity)){
                if(oncogenicity[m]=="Likely Oncogenic" | oncogenicity[m]=="Oncogenic"){
                   print(paste("Actionable alteration:", dataAnnotated[ind[m],5], "in cell line", colnames(dataCRISPRBreast)[j], "and gene", pathwayGenes[[i]][k]))
                    mutation[i,j] <- mutation[i,j] + 1
                }
              }
            }

            
          }
        }
      }
    }
  }
}
mutation <- as.data.frame(mutation)

pheatmap(as.matrix(mutation),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="# genes with actionable variant mutations")
```

BRAF      V600E in MDAMB231
BRAF      G464V  Trametinib in DU4475
pathway: RTK RAS

## Sensitivity

For each pathway and breast cancer cell line, we count the number of genes of the pathway, which are targets of CTRP drugs to which the cell line is sensitive to. A cell line is considered to be sensitive to a drug if the area above the curve provided in the CTRP data is above 16.3. Higher area above the curve means higher sensitivity to the drug.

### Load CTRP drug sensitivity data

```{r ctrp}
# load ctrp data
drugAct <- exprs(getAct(ctrpData::drugData))
# parse cell lines names
tmpDrugColnames <- colnames(drugAct)
for(i in 1:length(tmpDrugColnames)){
  tmpDrugColnames[i] <- paste(gsub("[-]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[ ]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[.]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[/]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[(]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[)]", "", toupper(tmpDrugColnames[i])), sep = "")
}
colnames(drugAct) <- tmpDrugColnames
indmatch <- match(colnames(drugAct),colnames(dataCRISPRBreast))
#drugAct <- drugAct[,!is.na(indmatch)]

tmp <- as.vector(drugAct[,!is.na(indmatch)])
tmp <- tmp[!is.na(tmp)]

myhist <- hist(tmp)
multiplier <- myhist$counts / myhist$density
mydensity <- density(tmp)
mydensity$y <- mydensity$y * multiplier[1]
plot(myhist,main="",xlab="sensitivity to drug",ylab="count",col="gray",ylim=c(0,6000))
lines(mydensity,lwd=2)
abline(v=16.3, col="red")
```

### Load targetable genes

```{r target}
targetableGenes <- readRDS("./data/targetableGenesExtended.rds")
```

### Fill table

Thresholding using 16.3
The threshold choice is the mean value of all the drug sensitivity values in all cell lines in CTRP.
A better way to distinguish sensitive versus resistant cell lines is to use "outlier" sensitivity values. See next section.

```{r sensitivity}
# initialization
sensitivity <- data.frame(row.names = names(pathwayGenes))
pairs <- NULL
for(i in 1:length(pathwayGenes)){
  for(j in 1:ncol(dataCRISPRBreast)){
    
    # if cell line is measured in CTRP
    if(length(which(colnames(drugAct)==colnames(dataCRISPRBreast)[j]))>0){
      sensitivity[i,j] <- 0
      
      for(k in 1:length(pathwayGenes[[i]])){
        
        # get names of drugs which target the gene in the pathway
        drugnames <- targetableGenes$drug[which(targetableGenes$gene == pathwayGenes[[i]][k])]
        tmpind <- match(unlist(drugnames),rownames(drugAct))
 
        flagSens <- 0
        if(length(tmpind[!is.na(tmpind)])>0){
          for(l in 1:length(tmpind[!is.na(tmpind)])){
            if(!is.na(drugAct[tmpind[l],colnames(dataCRISPRBreast)[j]])){
              # the cell line is considered sensitive if the area above the curve value in the CTRP data is above a particular threshold
              if(drugAct[tmpind[l],colnames(dataCRISPRBreast)[j]]>16.3){
                flagSens <- 1
                pairs <- c(pairs,paste(pathwayGenes[[i]][k],",",rownames(drugAct)[tmpind[l]]))
              }
            }
          }
        }
        
        # use the flag for sensitivity to not double count genes which have multiple drugs targeting it
        if(flagSens){
          sensitivity[i,j] <- sensitivity[i,j] + 1
        }else{
          #check also neighbors?
        }
      }
    }else{
      sensitivity[i,j] <- NA
    }
  }
}
colnames(sensitivity) <- colnames(dataCRISPRBreast)
rownames(sensitivity) <- rownamesTable
pheatmap(as.matrix(sensitivity),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="# genes targeted by CTRP drug \nwith sensitivity > 16.3")

sensitivityCutoff <- sensitivity
```

### Fill table

Thresholding using extremvalues
This method is used in the following paper
Speyer,G. et al, 2016. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5180601/

```{r sensitivity2}
library(extremevalues)
# load targetable genes
targetableGenes <- readRDS("./data/targetableGenesExtended.rds")

# initialization
sensitivity <- data.frame(row.names = names(pathwayGenes))
pairs <- NULL
for(i in 1:length(pathwayGenes)){
  for(j in 1:ncol(dataCRISPRBreast)){
    
    # if cell line is measured in CTRP
    if(length(which(colnames(drugAct)==colnames(dataCRISPRBreast)[j]))>0){
      sensitivity[i,j] <- 0
      
      for(k in 1:length(pathwayGenes[[i]])){
        
        # get names of drugs which target the gene in the pathway
        drugnames <- targetableGenes$drug[which(targetableGenes$gene == pathwayGenes[[i]][k])]
        tmpind <- match(unlist(drugnames),rownames(drugAct))
        flagSens <- 0
        if(length(tmpind[!is.na(tmpind)])>0){
          for(l in 1:length(tmpind[!is.na(tmpind)])){
            if(!is.na(drugAct[tmpind[l],colnames(dataCRISPRBreast)[j]])){
              tmp <- getOutliers(drugAct[tmpind[l],])
              # the cell line is considered sensitive if it is classified as right outlier by extremevalues R packages
              if(length(which(!is.na(match(colnames(dataCRISPRBreast)[j],names(tmp$iRight)))))>0){
                flagSens <- 1
                pairs <- c(pairs,paste(pathwayGenes[[i]][k],",",rownames(drugAct)[tmpind[l]]))
              }
            }
          }
        }
        
        # use the flag for sensitivity to not double count genes which have multiple drugs targeting it
        if(flagSens){
          sensitivity[i,j] <- sensitivity[i,j] + 1
        }else{
          #check also neighbors?
        }
      }
    }else{
      sensitivity[i,j] <- NA
    }
  }
}
colnames(sensitivity) <- colnames(dataCRISPRBreast)
rownames(sensitivity) <- rownamesTable
pheatmap(as.matrix(sensitivity),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="# genes targeted by CTRP drug \nwith sensitivity (outlier)")
```

pluripotin RASAL1
SMER-3 CUL1
PF-4800567 hydrochloride CSNK1D CSNK1E

```{r plotsensall}
plotdrugs <- c("MK-2206","erlotinib","MK-2206")
boxplot(t(drugAct[plotdrugs,]), outpch = NA)
stripchart(drugAct[plotdrugs,], vertical = TRUE, method = "jitter", pch = 21, add = TRUE)
```


```{r plotsens1}
myhist <- hist(as.numeric(drugAct["MK-2206",]))
multiplier <- myhist$counts / myhist$density
mydensity <- density(as.numeric(drugAct["MK-2206",])[!is.na(drugAct["MK-2206",])])
mydensity$y <- mydensity$y * multiplier[1]

plot(myhist,col="gray",ylim=c(0,max(mydensity$y)), ylab="cell line count",xlab="sensitivity to MK-2206",main="")
lines(mydensity,lwd=2)

tmp <- getOutliers(drugAct["MK-2206",])
abline(v=min(drugAct["MK-2206",names(tmp$iRight)]),col="red")


DATA = as.numeric(drugAct["MK-2206",])
boxplot(DATA, outpch = NA)
stripchart(DATA, vertical = TRUE, method = "jitter", pch = 21, add = TRUE)
```

```{r plotsens2}
myhist <- hist(as.numeric(drugAct["erlotinib",]))
multiplier <- myhist$counts / myhist$density
mydensity <- density(as.numeric(drugAct["erlotinib",])[!is.na(drugAct["erlotinib",])])
mydensity$y <- mydensity$y * multiplier[1]

plot(myhist,col="gray",ylim=c(0,max(mydensity$y)), ylab="count",xlab="sensitivity to erlotinib",main="")
lines(mydensity,lwd=2)

tmp <- getOutliers(drugAct["erlotinib",])
abline(v=min(drugAct["erlotinib",names(tmp$iRight)]),col="red")

DATA = as.numeric(drugAct["erlotinib",])
boxplot(DATA, outpch = NA)
stripchart(DATA, vertical = TRUE, method = "jitter", pch = 21, add = TRUE)
```

```{r plotsens3}
myhist <- hist(as.numeric(drugAct["nutlin-3",]))
multiplier <- myhist$counts / myhist$density
mydensity <- density(as.numeric(drugAct["nutlin-3",])[!is.na(drugAct["nutlin-3",])])
mydensity$y <- mydensity$y * multiplier[1]

plot(myhist,col="gray",ylab="cell line count",xlab="sensitivity to nutlin-3",main="")
lines(mydensity,lwd=2)

tmp <- getOutliers(drugAct["nutlin-3",])
abline(v=min(drugAct["nutlin-3",names(tmp$iRight)]),col="red")

DATA = as.numeric(drugAct["nutlin-3",])
boxplot(DATA, outpch = NA)
stripchart(DATA, vertical = TRUE, method = "jitter", pch = 21, add = TRUE)
```
## Essentiality

For each pathway and breast cancer cell line, we count the number of genes which are essential (i.e. the CERES score is below -0.7)

```{r essentiality}
isEssentialBreast <- dataCRISPRBreast
isEssentialBreast[which(dataCRISPRBreast > -0.7,arr.ind=T)] <- 0
isEssentialBreast[which(dataCRISPRBreast <= -0.7,arr.ind=T)] <- 1

essentiality <- data.frame(row.names = names(pathwayGenes))

for(i in 1:length(pathwayGenes)){
  for(j in 1:ncol(dataCRISPRBreast)){
    essentiality[i,j] <- 0
    for(k in 1:length(pathwayGenes[[i]])){
      if(!is.na(isEssentialBreast[pathwayGenes[[i]][k],j])){
        essentiality[i,j] <- essentiality[i,j]+isEssentialBreast[pathwayGenes[[i]][k],j] 
      }
    }
  }
}
colnames(essentiality) <- colnames(dataCRISPRBreast)
rownames(essentiality) <- rownamesTable
pheatmap(as.matrix(essentiality),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="# essential genes")
```

## Correlation

For each pathway and breast cancer cell line, we count the number of genes of the pathway, which are targets of CTRP drugs and for which essentiality and drug sensitivity has a correlation coefficient above 0.4. The correlations were calculated in crispr_drug.Rmd over all cancer cell lines. Therefore, the gene count is not cell line specific and we assign the same value to all breast cell lines. 

```{r correlation}
pairNeighborFile <- "./data/matchedPairsNeigborExtended.Rds"
matchedPairs <- readRDS(pairNeighborFile)

correlation <- data.frame(row.names = names(pathwayGenes))

for(i in 1:length(pathwayGenes)){
  for(j in 1:ncol(dataCRISPRBreast)){
    correlation[i,j] <- 0
    for(k in 1:length(pathwayGenes[[i]])){
      tmpind <- which(matchedPairs$gene==pathwayGenes[[i]][k])
      if(length(which(matchedPairs$corr[tmpind]>0.4))>0){
        correlation[i,j] <- correlation[i,j] + 1
      }
    }
  }
}
colnames(correlation) <- colnames(dataCRISPRBreast)
rownames(correlation) <- rownamesTable
pheatmap(as.matrix(correlation),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="# genes with correlation \nessentiality/drug sensitivity > 0.4")
```

## ALMANAC drugs

```{r ALMANAC}
scoreFile <- "./data/dataALMANACPaired.rds"
dataALMANACPaired <- readRDS(scoreFile)

drugCombinations <- matrix(0,10,3)
colnames <- c("MDAMB231","HS578T","BT549")
idComb1 <- NULL
idComb2 <- NULL
idComb3 <- NULL

for(i in 1:nrow(drugCombinations)){
  for(k in 1:length(pathwayGenes[[i]])){
      tmpind <- union(grep(paste("\\b",pathwayGenes[[i]][k],"\\b",sep=""),dataALMANACPaired$targetA), grep(paste("\\b",pathwayGenes[[i]][k],"\\b",sep=""),dataALMANACPaired$targetB))
      flag1 <- FALSE
      flag2 <- FALSE
      flag3 <- FALSE
      if(length(tmpind)>0){
        for(l in 1:length(tmpind)){
          if(!is.na(dataALMANACPaired$MDAMB231[tmpind[l]]) & dataALMANACPaired$MDAMB231[tmpind[l]]>0){
            flag1 <- TRUE
            idComb1 <- c(idComb1,tmpind[l])
          }
          if(!is.na(dataALMANACPaired$HS578T[tmpind[l]]) & dataALMANACPaired$HS578T[tmpind[l]]>0){
            flag2 <- TRUE
            idComb2 <- c(idComb2,tmpind[l])
          }
          if(!is.na(dataALMANACPaired$BT549[tmpind[l]]) & dataALMANACPaired$BT549[tmpind[l]]>0){
            flag3 <- TRUE
            idComb3 <- c(idComb3,tmpind[l])
          }
        }
      }
      if(flag1){drugCombinations[i,1] <- drugCombinations[i,1]+1}
      if(flag2){drugCombinations[i,2] <- drugCombinations[i,2]+1}
      if(flag3){drugCombinations[i,3] <- drugCombinations[i,3]+1}
    }
}

rownames(drugCombinations) <- rownamesTable

pheatmap(as.matrix(drugCombinations),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="effective drug combinations\nin ALMANAC")
```

```{r tableALMANAC}
DT::datatable(dataALMANACPaired[union(idComb1,union(idComb2,idComb3)),])
```

## Neighbors

We count the number of downstream neighbors of the genes in the pathways. Since this is again not cell line specific, we assign the same value to each cell lines. The pathway neighbors are extracted in crispr_analysis.Rmd.

```{r pathwayneighbors}
# load pathway neighbors
pwNeighborsFile <- "./data/pathwayNeighbors.rds"
pathwayNeighbors <- readRDS(pwNeighborsFile)

numNeighbors <- as.matrix(lapply(pathwayNeighbors,length))
neighbors <- matrix(0,nrow(mutation),ncol(mutation))
for(i in 1:ncol(mutation)){
  for(j in 1:nrow(mutation)){
    neighbors[j,i] <- as.numeric(numNeighbors[j,1])
  }
}
colnames(neighbors) <- colnames(dataCRISPRBreast)
rownames(neighbors) <- names(pathwayGenes)

pheatmap(as.matrix(neighbors),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="neighbors",fontsize_number = 5)
```

# Plot all tables

```{r plot, fig.height = 1.5, fig.width=3, eval=FALSE}
pheatmap(as.matrix(mutation),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="actionable mutations")
pheatmap(as.matrix(sensitivity),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="drug sensitivity ")
pheatmap(as.matrix(essentiality),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="essentiality")
pheatmap(as.matrix(correlation),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="correlation")
pheatmap(as.matrix(neighbors),cluster_cols = FALSE, cluster_rows = FALSE,display_numbers=TRUE, number_format ="%i",cellwidth = 10, cellheight = 10, main="neighbors",fontsize_number = 5)
```

mutations in general
check drugs
  AZD6482 (affects PIK3CB and PIK3CD)
  fulvestrant

check numbers correlation

AKT1 mutation E17K

filter drug sens with essentiality


# Session Info

```{r sessioninfo}
sessionInfo()
```