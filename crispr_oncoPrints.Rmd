---
title: "CRISPR Oncoprints"
output:
  html_notebook: default
---

# Purpose

Match CRISPR data to CCLE data and generate oncoprints for each of the 10 TCGA pathways

```{r knitrSetup, include=FALSE}
library(knitr)
opts_chunk$set(out.extra='style="display:block; margin: auto"', fig.align="center", tidy=FALSE)
verbose <- FALSE
```

# Load libraries

```{r loadLibraries, message=FALSE}
library(ggplot2)
library(ComplexHeatmap)
library(VennDiagram)
library(circlize)

library(rcellminer)
library(ctrpData)
```

# Load Achilles CRISPR data

```{r loadDataCRISPR}
# (crisprDataDir folder path necessary)
# crisprDataDir <- "/Users/user/Dropbox/ludwig_crispr"
source('./R/getCRISPRdata.R')
```

# Load CCLE data 

```{r loadDataCCLE}
# read CNA data
CCLEFile <- file.path(crisprDataDir, 'cellline_ccle_broad/data_CNA.txt')
dataCCLE <- read.table(CCLEFile,header = TRUE,sep = "\t",stringsAsFactors = FALSE)
# add genenames as rownames
rownames(dataCCLE) <- dataCCLE$Hugo_Symbol
dataCCLE <- dataCCLE[,-c(1,2,3)]
# with tissue name added to cell line name
cclnamesCCLE <- tail(colnames(dataCCLE),-3) 

# without tissue name in name
colnames(dataCCLE) <- gsub("_.*","",colnames(dataCCLE))

# read mutation data
CCLEMutFile <- file.path(crisprDataDir, 'cellline_ccle_broad/data_mutations_extended.txt')
dataMUT <- read.table(CCLEMutFile,header = TRUE,sep = "\t",fill = TRUE,stringsAsFactors = FALSE,quote = "",skip = 1, comment.char = "")
```

# Visualize overlapping cell lines and genes

## Cell lines

```{r vennCCL,fig.height=1}
v <- venn.diagram(list(Achilles=rownames(dataCRISPR), CCLE=rownames(dataCCLE)), filename=NULL,fill=rainbow(2), main="cell lines")
grid.newpage()
grid.draw(v)
```

## Genes

```{r venngene,fig.height=1}
v <- venn.diagram(list(Achilles=colnames(dataCRISPR), CCLE=cclnamesCCLE), filename=NULL,fill=rainbow(2), main = "genes")
grid.newpage()
grid.draw(v)
```

## Extract cell lines of interest

```{r cellines}
dataCRISPR <- dataCRISPR[,which(cclsCRISPR$tissue=="BREAST")]
print(paste(dim(dataCRISPR)[2], "Breast cancer cell lines"))
```

# Match cell lines Achilles - CCLE

```{r matchCCLECRISPR}
# match gene names
indmatchgene <- match(rownames(dataCRISPR),rownames(dataCCLE))
indmatchccl <- match(colnames(dataCRISPR),colnames(dataCCLE))

dataCCLE <- dataCCLE[indmatchgene[!is.na(indmatchgene)],indmatchccl[!is.na(indmatchccl)]]

dataCRISPR <- dataCRISPR[!is.na(indmatchgene),]

cclnames <- colnames(dataCRISPR)
genenames <- rownames(dataCRISPR)
nGenes <- length(genenames)
```

## Get essential genes (essential in ALL considered cell)

Essentiality cutoff of -0.7

```{r essential}
isEssential <- dataCRISPR
isEssential[which(dataCRISPR > -0.7,arr.ind=T)] <- 0
isEssential[which(dataCRISPR <= -0.7,arr.ind=T)] <- 1
essentialGenes <- rownames(dataCRISPR)[which(rowSums(isEssential)==ncol(isEssential))]
print(paste(length(essentialGenes),"genes essential in all Breast cancer cell lines"))
```

# Load annotated variants 

File from Precision Oncology Knowledge Base http://oncokb.org

```{r loadannotatedvariant}
dataAnnotated <- read.table('./data/allAnnotatedVariants.txt',header = TRUE,sep = "\t",fill = TRUE,stringsAsFactors = FALSE,quote = "", comment.char = "")
```

# Fill matrix with CCLE information 

```{r fillmat}
matFile <- "./data/matOncoPrintAllBreastCancer.rds"
if(file.exists(matFile)) {
  matOncoPrint <- readRDS(matFile)
} else {
  matOncoPrint <- matrix(NA, nrow = nGenes, ncol = ncol(dataCCLE))
  colnames(matOncoPrint) <- cclnames
  rownames(matOncoPrint) <- genenames
  
  colMUT = which(colnames(dataMUT)=='Variant_Classification')
  colMAFImpact = which(colnames(dataMUT)=='MA.FImpact')
  colChange = which(colnames(dataMUT)=='amino_acid_change') 
  colCellline = which(colnames(dataMUT) == "Tumor_Sample_Barcode")
  
  # rename cell lines 
  dataMUT[,colCellline] <- gsub("_.*","", dataMUT[,colCellline])
  
  # only include the following mutations
  checkMUT = c("Frame_Shift_Del","Frame_Shift_Ins","In_Frame_Del","In_Frame_Ins","Nonstop_Mutation","Splice_Site","Splice_Region","Translation_Start_Site","Nonsense_Mutation","Missense_Mutation")
  
  for(iSample in 1:length(cclnames)) {
      print(iSample)
      for(iGene in 1:nGenes) {
        matOncoPrint[iGene,iSample] <- ''
        
        # check for certain mutations
        flagMUT <- 0
        iRows <- which(dataMUT[,1]==genenames[iGene] & dataMUT[,colCellline]==cclnames[iSample])
        if(length(iRows) > 0){
          # check the mutation types besides Missense_Mutation (checked below)
          for(i in 1:9) {
            if(checkMUT[i] %in% dataMUT[iRows,colMUT]) {flagMUT <- 1}
          }
        }
        
        # additionally filter Missense_Mutation
        if(flagMUT == 0 & ("Missense_Mutation" %in% dataMUT[iRows,colMUT])) {
          
          # only include mutation if MAFImpact is high
          rowMissense = which(dataMUT[iRows,colMUT]=="Missense_Mutation")
          if(length(which(dataMUT[iRows[rowMissense],colMAFImpact]=="high")>0)) { flagMUT <- 1 }
          
          # include mutations that are classified as likely ongogenic or oncogenic in the Precision Oncology Knowledge Base 
          mutations <- dataMUT[iRows[rowMissense],colChange]
          for(i in 1:length(mutations)){
            oncogenicity <- dataAnnotated[which(dataAnnotated[,4]==genenames[iGene] & paste("p.",dataAnnotated[,5],sep="") == mutations[i]),6]
            if(length(oncogenicity)>0){
              for(j in 1:length(oncogenicity)){
                if(oncogenicity[j]=="Likely Oncogenic" | oncogenicity[j]=="Oncogenic"){
                  flagMUT <- 1
                }
              }
            }
          }
        }

        if(flagMUT) {matOncoPrint[iGene,iSample] <- 'MUT'}
        
        # add copy number variation information, amplification if 2
        if(dataCCLE[iGene,iSample]==2) {
          if(matOncoPrint[iGene,iSample]=='') {matOncoPrint[iGene,iSample] <- paste(matOncoPrint[iGene,iSample],'AMP',sep="")}
          else { matOncoPrint[iGene,iSample] <- paste(matOncoPrint[iGene,iSample],';AMP',sep="")}
        }
        # deletion if -2 
        if(dataCCLE[iGene,iSample]==-2) {
          if(matOncoPrint[iGene,iSample]=='') {matOncoPrint[iGene,iSample] <- paste(matOncoPrint[iGene,iSample],'DEL',sep="")}
          else { matOncoPrint[iGene,iSample] = paste(matOncoPrint[iGene,iSample],';DEL',sep="")}
        }
        
        # add information about essentiality
        if(isEssential[iGene,iSample]==1) {
          if(matOncoPrint[iGene,iSample]=='') {
            matOncoPrint[iGene,iSample] <- paste(matOncoPrint[iGene,iSample],'ESS',sep="")}
          else { 
            matOncoPrint[iGene,iSample] <- paste(matOncoPrint[iGene,iSample],';ESS',sep="")}
        }
    }
  }
  saveRDS(matOncoPrint, file = matFile)
}
```

# Load TCGA pathway genes

```{r pathwayGenes}
pathwayGenes <- readRDS(file.path(crisprDataDir, "Rdata/pathwayGenes.rds"))

# A list of genes important in breast cancer 
#pathwayGenes$custom <- c("ATM", "BARD1", "BRCA1", "BRCA2", "BRIP1", "CDH1", "CHEK2", "NBN", "NF1", "PALB2", "PTEN", "RAD50", "STK11", "TP53", "AKT1", "FAM175A", "FANCC", "MRE11", "MUTYH", "PIK3CA", "RAD51C", "RAD51D", "RINT1", "SDHB", "SDHD", "XRCC2")
```

# Generate OncoPrints with essentiality, mutation and copy number variation

## For each pathway seperately
```{r oncoprintEss, fig.width=6, fig.height=13}
col = c("MUT" = "#008000", "AMP" = "red", "DEL" = "blue", "ESS" = "yellow")
oncoprints <- list()

for(iPathway in 1:length(pathwayGenes)) {
  png(paste("./figures/allBreast/oncoprint_",names(pathwayGenes)[iPathway],".png", sep=""),width = 600, height = 700)
  indmatchgenes <- match(pathwayGenes[[iPathway]],genenames)
  oncoprints[[iPathway]] <- oncoPrint(matOncoPrint[indmatchgenes[!is.na(indmatchgenes)],], get_type = function(x) strsplit(x, ";")[[1]],
    alter_fun = list(
        AMP = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.9, gp = gpar(fill = col["AMP"], col = NA)),
        DEL = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.7, gp = gpar(fill = col["DEL"], col = NA)), 
        MUT = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.5, gp = gpar(fill = col["MUT"], col = NA)), 
        ESS = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.3, gp = gpar(fill = col["ESS"], col = NA))), 
    col = col, show_column_names = TRUE,
    column_title = names(pathwayGenes)[iPathway],
    heatmap_legend_param = list(title = "Alterations", at = c("AMP", "DEL", "MUT","ESS"), 
        labels = c("Amplification", "Deletion", "Mutation","Essential")))
  print(oncoprints[[iPathway]])
  dev.off()
}
```

## For all pathway genes combined
```{r oncoprintEssall, fig.width=6, fig.height=18}
col = c("MUT" = "#008000", "AMP" = "red", "DEL" = "blue", "ESS" = "yellow")

png(paste("./figures/allBreast/oncoprint_allpathwaygenes.png", sep=""), width = 1200, height = 3600)
indmatchgenes <- match(unlist(pathwayGenes),genenames)
oncoprint <- oncoPrint(matOncoPrint[indmatchgenes[!is.na(indmatchgenes)],], get_type = function(x) strsplit(x, ";")[[1]],
alter_fun = list(
    AMP = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.9, gp = gpar(fill = col["AMP"], col = NA)),
    DEL = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.7, gp = gpar(fill = col["DEL"], col = NA)), 
    MUT = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.5, gp = gpar(fill = col["MUT"], col = NA)), 
    ESS = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.3, gp = gpar(fill = col["ESS"], col = NA))), 
col = col, show_column_names = TRUE,
column_title = "Combined TCGA Pathways",
heatmap_legend_param = list(title = "Alterations", at = c("AMP", "DEL", "MUT","ESS"), 
    labels = c("Amplification", "Deletion", "Mutation","Essential")))
print(oncoprint)
dev.off()

```

## With annotation for PI3K

### Sensitivity to sirolimus

```{r sirolimus}
drugAct <- exprs(getAct(ctrpData::drugData))
tmpDrugColnames <- colnames(drugAct)
for(i in 1:length(tmpDrugColnames)){
  tmpDrugColnames[i] <- paste(gsub("[-]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[ ]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[.]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[/]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[(]", "", tmpDrugColnames[i]), sep = "")
  tmpDrugColnames[i] <- paste(gsub("[)]", "", toupper(tmpDrugColnames[i])), sep = "")
}
colnames(drugAct) <- tmpDrugColnames

annotateSens <- drugAct["sirolimus",match(colnames(matOncoPrint),colnames(drugAct))]
```

### Oncoprint 
```{r oncoprintPi3K, fig.width=5, fig.height=4}
col = c("MUT" = "#008000", "AMP" = "red", "DEL" = "blue", "ESS" = "yellow")

ha = HeatmapAnnotation(data.frame(DrugSensitivity = annotateSens, TNBC = factor((cclsCRISPR$addInfo[match(colnames(matOncoPrint),cclsCRISPR$name)]=="TNBC"), levels = c(TRUE,FALSE), labels = c("TRUE", "FALSE"))),col = list(DrugSensitivity = colorRamp2(c(9.3, 23.3), c("blue","red")),TNBC = c("TRUE" = "white","FALSE" = "black")),na_col = "grey")
	
indmatchgenes <- match(pathwayGenes[[6]],genenames)
oncoprint <- oncoPrint(matOncoPrint[indmatchgenes[!is.na(indmatchgenes)],], get_type = function(x) strsplit(x, ";")[[1]],
alter_fun = list(
    AMP = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.9, gp = gpar(fill = col["AMP"], col = NA)),
    DEL = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.7, gp = gpar(fill = col["DEL"], col = NA)), 
    MUT = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.5, gp = gpar(fill = col["MUT"], col = NA)), 
    ESS = function(x, y, w, h) grid.rect(x, y, w*0.9, h*0.3, gp = gpar(fill = col["ESS"], col = NA))), 
col = col, show_column_names = TRUE,
column_title = names(pathwayGenes)[6],
heatmap_legend_param = list(title = "Alterations", at = c("AMP", "DEL", "MUT","ESS"), 
    labels = c("Amplification", "Deletion", "Mutation","Essential")), bottom_annotation = ha)
draw(oncoprint)
```

# Session Info

```{r sessioninfo}
sessionInfo()
```
