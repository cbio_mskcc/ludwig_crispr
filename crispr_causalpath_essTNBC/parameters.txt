proteomics-values-file = ess_TNBC.txt
id-column = ID
symbols-column = Symbols
sites-column = Sites
effect-column = Effect

value-transformation = difference-of-means
test-value-column = TNBC
control-value-column = compareTo
color-saturation-value = 0.5

calculate-network-significance = true
permutations-for-significance = 10000
threshold-for-data-significance = 0.01 phosphoprotein
threshold-for-data-significance = 0.01 protein
